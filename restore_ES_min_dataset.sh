#!/bin/bash

ES_ENDPOINT=http://localhost:9200
BUCKET_NAME=whalar-es-minimum-dataset
#LAST=$(date -d "last Sunday" +"%Y-%m-%d")
LAST="latest"

if [ -z "$1" ] && [ -z "$2" ]; then
    echo "AWS_ACCES_KEY: "
    read ACCESS_KEY_ID
    echo "AWS_SECRET_KEY: "
    read SECRET_ACCESS_KEY
else
    ACCESS_KEY_ID=$1
    SECRET_ACCESS_KEY=$2
fi

# docker-compose stop elastic
# docker-compose up -d elastic

# sleep 15

# if [ -z `docker ps -q --no-trunc | grep $(docker-compose ps -q elastic)` ]; then
#   echo "ERROR, elastic is not running."
#   return 1
# else
#   echo "Yes, it's running."
# fi

echo ""
echo "Removing old dataset snapshot"
curl -XDELETE "$ES_ENDPOINT/_snapshot/dataset"
sleep 15

echo ""
echo "Registering last minimun dataset snapshot"
curl -XPUT "$ES_ENDPOINT/_snapshot/dataset?pretty" -H 'Content-Type: application/json' -d'
{
 "type": "s3",
 "settings": {
   "bucket": "'"$BUCKET_NAME"'",
   "region": "eu-west-1",
   "access_key": "'"$ACCESS_KEY_ID"'",
   "secret_key": "'"$SECRET_ACCESS_KEY"'",
   "base_path": "'"$LAST"'",
   "role_arn": "arn:aws:iam::773676435453:role/whalaressnapshots"
 }
}'

echo ""
echo "taking snapshot name"
SNAPSHOT_NAME=$(curl -s $ES_ENDPOINT/_snapshot/dataset/_all?pretty | grep "snapshot\" :" | cut -f 4 -d '"')
echo "SNAPSHOT_NAME: " $SNAPSHOT_NAME

# echo ""
# echo "Checking SNAPSHOT NAME IS CORRECT"
# curl -XGET $ES_ENDPOINT/_snapshot/dataset/$SNAPSHOT_NAME?pretty 

echo ""
echo "closing all index \n"
curl -XPOST "$ES_ENDPOINT/_all/_close" 

echo ""
echo "Running restore process..."
curl -XPOST "$ES_ENDPOINT/_snapshot/dataset/$SNAPSHOT_NAME/_restore?pretty" 

echo ""
echo "Checking recovery process"
i=90
curl -s -XGET "$ES_ENDPOINT/_cat/recovery" | grep content | grep -E '(100.0.*){3}'
CHECK=$?
while [ $CHECK -ne 0 ] && [ $i -gt 0 ]
do
    sleep 5
    curl -s -XGET "$ES_ENDPOINT/_cat/recovery" | grep content | grep -E '(100.0.*){3}' > /dev/null
    CHECK=$?
    echo -n "."
    i=$(($i - 1 ))
done

echo ""
echo DONE!!!